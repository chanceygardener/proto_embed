#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include <unordered_map>
#include "/home/chanceygardener/repos/proto_embed/src/tokenize/tokenize.h"
#include "/home/chanceygardener/repos/proto_embed/src/wordembedder/wordembedder.h"
#include "/home/chanceygardener/repos/proto_embed/src/utils/utils.h"

using namespace std;

WordEmbedder::WordEmbedder(string corpusName)
	: corpusName(corpusName) {}
		// methods to get and set corpus name

		string WordEmbedder::getCorpusName () {
			string corpusName = corpusName;
			return corpusName;
		}
		vector<string> WordEmbedder::_getSchema() {
            return schema;
        }
		void WordEmbedder::setCorpusName(string& name) {
            corpusName = name;
        }
		// tokenize method
		vector<string>WordEmbedder::tokensFromFile(int fcount, char* fnames[]) {
            cout << "\ntokenizeing...\n" << endl;
			vector<string> tokens;
            tokenize(fnames, fcount,tokens);
            cout << "\nupdating schema\n" << endl;
            _updateSchema(tokens);
            cout << tokens.size() << "\n" << endl;
			return tokens;
		}

        void
        WordEmbedder::getTokensInContext(vector<string>& tokens, int window_size) {
            // declare skipgram and word environment maps
            // get skipgram embeddings for each word in token
            for (int idx = 0; idx != tokens.size(); ++idx) {
                string tok = normalize(tokens[idx]);
                auto lookup = context_buffer.find(tok);
                // make a key in the dist mapping if it's not in there yet
                if (lookup == context_buffer.end()) {
                    context_buffer.insert(make_pair(tok,
                                              vector<vector<string>>(1,
                                                      getWindow(tokens, window_size, idx) )));
                }
            }
        }

        unordered_map<string,vector<double>>
        WordEmbedder::tokensToSkipgram
                (vector<string>& tokens, int window_size) {
            cout << "\ncompiling token contexts\n" << endl;
            // the line below updates the schema
            // without updating vocab size...
            getTokensInContext(tokens, window_size);
            cout << "\ngenerating co-occurrence vectors\n" << endl;
            sort(schema.begin(), schema.end());
            // the following block is iterating over previously accumulated contexts
            // of unique tokens. Therefore this block's steps should be
            // independent and potentially executed in parallel.
            int ind;
            int schidx;
            // wait  does widx == schidx?  should it?
            // no, schidx is for words in env, widx is for the word
            // being processed. Name your variables better, bruh
            vector<double> n_hot;
            for (int widx = 0; widx != vocab_size; ++widx) {
                n_hot = vector<double>(vocab_size, 0);
                // vocab lookup
                string& w = schema[widx];
                auto wlook = context_buffer.find(w);
                // at the 29th iteration of the loop above, this loop fails. why?
                vector<vector<string> > wscope = wlook->second;
                // per instance word
                int instance_count = 0;
                for (const vector<string>& en: wscope) {
                    // per word in instance
                    int wc_check = 0;
                    for (const string& neighbor : en) {
                        auto schema_it = find(schema.begin(),schema.end(), neighbor);
                        schidx = distance(schema.begin(), schema_it);
                        if (schidx > vocab_size) {
                            cout << "oh noo..." << endl;
                        }
                        n_hot[schidx] += 1.0;
                        wc_check++;
                    }
                    instance_count++;   // does this == wscope.size()? i think so
                }
                // divide array
                for (int i = 0; i < vocab_size; i++) {
                    if (n_hot[i] != 0) {
                        n_hot[i] = n_hot[i] / (instance_count);
                    }
                }
                //vector<double> skipgram_entry = n_hot;
                if (widx == 976) {
                    cout << "test" << endl;
                }
                prob_buffer.insert({w, n_hot});
            }
            context_buffer.clear();
            return prob_buffer;
        }

		// skipgram extractor (takes vector)
		unordered_map<string,vector<double> >
			WordEmbedder::skipgram (vector<string>& tokens, int wsize) {
                    cout << "\n\ncompiling skipgram vectors...\n\n" << endl;
					unordered_map<string,vector<double>>
							out = tokensToSkipgram (tokens, wsize);
				return out;
		}

        void WordEmbedder::_updateSchema(vector<string> tokens) {
            for (string token: tokens) {
                string ntok = normalize(token);
                vector<string>::iterator lookup =
                        find(this->schema.begin(), this->schema.end(), ntok);
                if (lookup == this->schema.end()) {
                    this->schema.push_back(ntok);
                }
            }
        vocab_size = schema.size();
        }

    unordered_map<string,vector<string>>
    WordEmbedder::getContextsByToken(vector<string>& tokens, int window_size) {
        unordered_map<string,vector<string>> cont_inst;
        for (int idx = 0; idx != tokens.size(); ++idx) {
            vector<string> window(window_size*2);
            string center_word = tokens[idx];
            string context = catEnv(getWindow(tokens, window_size, idx));
            auto lookup = cont_inst.find(context);
            if (lookup == cont_inst.end()) {
                vector<string> centry = vector<string>(1, center_word);
                cont_inst.insert( make_pair(context,  centry) );
            }
            else {
                lookup->second.push_back(center_word);
            }
        }
        return cont_inst;
    }


void WordEmbedder::writeSkipgramsToCsv(string& ofname,
                unordered_map<string,vector<double>> dat) {

            ofstream of;
            cout << "\nwriting to csv\n" << endl;
            of.open(ofname);
            of << ","; // so the indices line up in the file.
            for (pair<string, vector<double> > token: dat) {
                of << token.first << ",";
            }
            of << "\n";
            for (pair<string, vector<double> > token: dat) {
                of << token.first << ",";
                for (double sgval: token.second) {
                    of << sgval << ",";
                }
                of << "\n";
            }
            of.close();
        }




