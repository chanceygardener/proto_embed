#include <Eigen/Dense>
#include <vector>
#include <algorithm>
#include <random>
#include <cmath>
#include "/home/chanceygardener/repos/proto_embed/src/utils/utils.h"
#include "fake_task.h"


using namespace std;
using namespace Eigen;

LinearNeuron::
LinearNeuron(int num_inputs, double weight) {
    // initiate weights at random values (uniform dist for now)
}

    double LinearNeuron::output(vector<double> input_vals) {
         double out = 0;
        for (int i = 0; i < input_vals.size(); i++) {
            double weight = input_weights[i];
            double ival = input_vals[i];
            double prod = weight * ival;
            out += prod;
        }
        return out;

 }


    // TODO( maybe just have this compute for the nonzero value rather than fucking iterate...
    // TODO( <continued> since it's a 1-hot vector going through this fucker.
//    vector<vector<double>>
//        getHidden(vector<vector<double>> cprobs, int schidx, int hidden_size) {
//        double num_inputs = cprobs.size();
//        vector<int> n_hot_in = vector<int>(num_inputs, 0);
//        n_hot_in[schidx] = 1;
//        vector<LinearNeuron> hidden;
//        for (int i = 0; i < hidden_size; i++) {
//            LinearNeuron hnode = LinearNeuron(num_inputs);
//            hidden[i] = hnode;
//    }
//
//
//}
vector<double> NnLayer::GetValues() {
    vector<double> out;
    for (LinearNeuron neuron: values) {
        out.push_back(neuron.value);
    }
    return out;
}
NnLayer::NnLayer(int dims, vector<bool> inputs) {
    uniform_real_distribution<double> init_dist(0.0, 1.0);
    for (unsigned int idx; idx < dims; idx++) {
        values[idx] = LinearNeuron(inputs.size(), weight_init());
    }
}

vector<double> NnLayer::OutPut(vector<double> input) {
    vector<double> out = vector<double>();
    for (LinearNeuron neuron : values) {
        double nout = neuron.output(GetValues());
        out.push_back(nout);
}
    return out;
}

double NnLayer::weight_init() {
    return init_dist(weight_init_gen);
}

vector<double>
NnLayer::weight_vector_init(int len) {

    vector<double> out;
    for (int i = 0; i < len; i++ ) {
        out[i] = weight_init();
    }
    return out;
}
